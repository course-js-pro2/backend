const mongoose = require('mongoose');
var Schema = mongoose.Schema;
var storeSchema = new Schema({
    title: String,
    text: String,
    img: String,
    cost: String,
    admin: String
});
const Store = mongoose.model("Store", storeSchema);
module.exports = Store