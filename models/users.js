const mongoose = require('mongoose');
const crypto = require('crypto')
var Schema = mongoose.Schema;
var UsersSchema = new Schema({
    username: {
        required: true,
        unique: true,
        type: String
    },
    passwordHash: String,
    salt: String,
    img: String,
    name: String,
    surname: String
}, {
    timestamps: true
});
UsersSchema.virtual('password').set(function(password){
    this.salt = crypto.randomBytes(256).toString('base64')
    this.passwordHash = crypto.pbkdf2Sync(password, this.salt, 1, 128, 'sha1')
})
UsersSchema.method('check_password', function(password){
    return this.passwordHash == crypto.pbkdf2Sync(password, this.salt, 1, 128, 'sha1')
})
const Users = mongoose.model("Users", UsersSchema);
module.exports = Users