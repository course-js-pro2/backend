var express = require('express');
var router = express.Router();
const Chats = require('../models/chats')
const {checkAuth} = require("../middleware/auth");

router.get('/', checkAuth, (req, res) => {
    Chats.find({users: {$in: req.decoded.data.username}}).then(c => {
        res.send(c)
    })
})
router.put('/', checkAuth, (req, res) => {
    let chats = new Chats(req.body)
    chats.save()
    res.send('ok')
})
router.post('/message', checkAuth, (req, res) => {
    Chats.updateOne({_id: req.body._id}, {$push: {messages: req.body.message}}).then(() => {res.send('ok')})
})
router.post('/change', checkAuth, (req, res) => {
    Chats.updateOne({_id: req.body._id}, {$set: req.body}).then(() => {res.send('ok')})
})
router.post('/get', checkAuth, (req, res) => {
    Chats.findOne({_id: req.body._id}).then(c => {
        res.send(c)
    })
})
router.delete('/:id', checkAuth, (req, res) => {
    Chats.deleteOne({_id: req.params.id}).then(() => {res.send('ok')})
})

module.exports = router;