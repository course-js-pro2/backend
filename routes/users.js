var express = require('express');
var router = express.Router();
const Users = require('../models/users')
const jwt = require('jsonwebtoken')
const fs = require('fs');
const {checkAuth} = require("../middleware/auth");

router.get('/all', checkAuth, (req, res) => {
    Users.find().select({'passwordHash': 0, 'salt': 0}).then(users => {
        res.send(users)
    })
})
router.get('/one', checkAuth, (req, res) => {
    Users.findOne({username: req.decoded.data.username}).select({'passwordHash': 0, 'salt': 0}).then(user => {
        res.send(user)
    })
})
router.post('/add', (req, res) => {
    let {username, password} = req.body
    let user = new Users({username, password})
    user.save().then(() => {
        res.send('ok')
    }).catch(err => {
        if (err.code == 11000) res.send('Данный пользователь уже существует, поменяйте username')
        else res.status('418').send('Error!')
    })
})
router.post('/change', checkAuth, (req, res) => {
    Users.findOne({username: req.body.username}).select('img').then(user => {
        if (user.img != '' && req.body.img != user.img) fs.unlinkSync(user.img)
    })
    Users.updateOne({username: req.body.username}, {$set: req.body}).then(() => {res.send('ok')}).catch(err => {res.status('418').send('Error!')})
})
router.delete('/', checkAuth, (req, res) => {
    let {username} = req.body
    Users.findOneAndDelete({username}).then(() => {res.send('ok')}).catch(err => {res.send('Error!')})
})

module.exports = router;